# import des bibliothèques
import tkinter as tk
import customtkinter as ctk
import tkinter.ttk as ttk
from tkinter import messagebox
import datetime
import locale

#import des fonctions
from connection import connexion_database
from requetes import lister_id_mouvement

#Defition de la langue en français
locale.setlocale(locale.LC_TIME, 'fr_FR.UTF-8')

connection = connexion_database()

# récupération du mois et de l'année en cours
annee_actuelle = datetime.datetime.now().year
mois_actuel = datetime.datetime.now().strftime("%B")

# Généreration d'une liste de 10 ans glissants
liste_annees = [str(year) for year in range(annee_actuelle - 5, annee_actuelle + 6)]


id_mouvement_suivant = lister_id_mouvement()
print(id_mouvement_suivant)


def ajouter_epargne():
    id = id_mouvement_suivant
    print(id_mouvement_suivant)
    montant = montant_entry.get()
    mois = month_combobox.get()
    annee = year_combobox.get()
    type = 3
    ajout_epargne_req = "INSERT INTO mouvement (id, moisMvt, anneeMvt, montant, libelle, id_type ) VALUES (%s, %s, %s, %s, 'Epargne', %s)"
    val = (id, mois, annee, montant, type)
    cursor = connection.cursor()
    cursor.execute(ajout_epargne_req, val)
    connection.commit()
    messagebox.showinfo("MESSAGE", "l'ajout est fait")
    cursor.close()
    form_epargne_window.destroy()

def form_ajout_epargne():
    global month_combobox, year_combobox, libelle_entry, montant_entry, id_mouvement_suivant, form_epargne_window

    form_epargne_window = tk.Toplevel()
    id_mouvement_suivant = lister_id_mouvement()
    print(id_mouvement_suivant)
    form_epargne_window.title("Ajout epargne")
    form_epargne_window.geometry("500x500")
    form_epargne_window.config(background="#EAF2F8")
    Form_epargne = tk.Frame(form_epargne_window, width=500, height=500, bd=8, relief="raise")

    Form_epargne.grid_rowconfigure(0, weight=0)
    Form_epargne.grid_rowconfigure(1, weight=0)
    Form_epargne.grid_rowconfigure(2, weight=0)
    Form_epargne.grid_rowconfigure(3, weight=0)

    Form_epargne.grid_columnconfigure(0, weight=1, uniform="same_group")
    Form_epargne.grid_columnconfigure(1, weight=1, uniform="same_group")

    # Première ligne
    Ajoutepargne_label = tk.Label(form_epargne_window, text="formulaire d'ajout d'une épargne", anchor='w',  # w => ouest gauche
                                 bg="#2471A3", fg="white", font=("Arial", 20), padx=10, pady=10)
    Ajoutepargne_label.grid(column=0, row=0, columnspan=2)

    # 2ème ligne
    mois_label = tk.Label(form_epargne_window, text="Mois", anchor='w',  # w => ouest gauche
                          bg="#EAF2F8",  fg="#51525D", font=("Arial", 14), padx=10, pady=10)
    mois_label.grid(column=0, row=1)

    months = ['janvier', 'fevrier', 'mars', 'avril', 'mai', 'juin', 'juillet', 'aout', 'septembre', 'octobre',
              'novembre', 'decembre']
    month_combobox = ttk.Combobox(form_epargne_window, values=months)
    month_combobox.grid(row=1, column=1, padx=10, pady=10, sticky="nsew")

    # 3ème ligne
    annee_label = tk.Label(form_epargne_window, text="Année", anchor='w',  # w => ouest gauche
                          bg="#EAF2F8",  fg="#51525D", font=("Arial", 14), padx=10, pady=10)
    annee_label.grid(column=0, row=2)

    year_combobox = ttk.Combobox(form_epargne_window, values=liste_annees)
    year_combobox.grid(row=2, column=1, padx=10, pady=10, sticky="nsew")

    # 3ème ligne

    # 4ème ligne
    montant_label = tk.Label(form_epargne_window, text="Montant", anchor='w',  # w => ouest gauche
                             bg="#EAF2F8",  fg="#51525D", font=("Arial", 14), padx=10, pady=10)
    montant_label.grid(column=0, row=4)

    montant_entry = tk.Entry(form_epargne_window)
    montant_entry.grid(column=1, row=4)

    # 5ème ligne
    valider_Bouton = ctk.CTkButton(form_epargne_window, text="Ajouter", command=lambda:ajouter_epargne())
    valider_Bouton.grid(column=0, row=5, columnspan=2)



    # On crée notre fenêtre et on l'affiche

    form_epargne_window.mainloop()

def ouvrir_form_ajout_epargne():
    form_ajout_epargne()

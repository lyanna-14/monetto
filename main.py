import tkinter as tk
import customtkinter as ctk
import tkinter.ttk as ttk
import datetime
import locale
from tkinter import messagebox

from ajout_revenu import ouvrir_form_ajout_revenu
from ajout_depense import ouvrir_form_ajout_depense
from ajout_epargne import ouvrir_form_ajout_epargne
from connection import connexion_database

#Defition de la langue en français
locale.setlocale(locale.LC_TIME, 'fr_FR.UTF-8')

#Defition de la langue en françaisocale.setlocale(locale.LC_TIME, 'fr_FR.UTF-8')
class MyApplication(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)

# ========================   Affichage des données dans les treeview ==========================================================================
    #Quand on clique sur le bouton afficher
    def afficher_donnees(self):
        selected_value1 = self.month_combobox.get()
        selected_value2 = self.year_combobox.get()
        #print(selected_value1)
        #print(selected_value2)
        #supprimer les donner les treeview
        app.revenuTreeview.delete(*app.revenuTreeview.get_children())
        app.depenseTreeview.delete(*app.depenseTreeview.get_children())
        app.epargneTreeview.delete(*app.epargneTreeview.get_children())
        #requetes
        revenu_req = "select id, moisMvt, anneeMvt, montant, libelle from mouvement mvt where mvt.id_type=1 and moisMvt=%s and anneeMvt=%s "
        depense_req ="select id, moisMvt, anneeMvt, montant, libelle from mouvement mvt where mvt.id_type=2 and moisMvt=%s and anneeMvt=%s "
        epargne_req = "select id, moisMvt, anneeMvt, montant from mouvement mvt where mvt.id_type=3 and moisMvt=%s and anneeMvt=%s "
        total_epargne_req="select SUM(montant) from mouvement where id_type=3 and moisMvt=%s and anneeMvt=%s"
        total_revenu_req="select SUM(montant) from mouvement where id_type=1 and moisMvt=%s and anneeMvt=%s"
        total_depense_req="select SUM(montant) from mouvement where id_type=2 and moisMvt=%s and anneeMvt=%s"
        solde_req="Select sum(T1.montant)from (select sum(montant) as montant from mouvement WHERE id_type=1 and moisMvt=%s and anneeMvt=%s UNION select -sum(montant) as montant from mouvement WHERE id_type=2 and moisMvt=%s and anneeMvt=%s UNION select -sum(montant) as montant from mouvement WHERE id_type=3 and moisMvt=%s and anneeMvt=%s) as T1"
        connection = connexion_database()
        cursor = connection.cursor()
        cursor.execute(revenu_req,(selected_value1,selected_value2))
        revenu_rows = cursor.fetchall()
        for revenu_row in revenu_rows:
            app.revenuTreeview.insert('', "end", values=(revenu_row[0], revenu_row[1], revenu_row[2], revenu_row[3], revenu_row[4] ))
        cursor.execute(depense_req,(selected_value1,selected_value2))
        depense_rows = cursor.fetchall()
        for depense_row in depense_rows:
            app.depenseTreeview.insert('', "end", values=(depense_row[0], depense_row[1], depense_row[2], depense_row[3], depense_row[4] ))
        cursor.execute(epargne_req, (selected_value1, selected_value2))
        epargne_rows = cursor.fetchall()
        for epargne_row in epargne_rows:
            app.epargneTreeview.insert('', "end", values=(
            epargne_row[ 0 ], epargne_row[ 1 ], epargne_row[ 2 ], epargne_row[ 3 ]))
        #Somme des épargnes
        cursor.execute(total_epargne_req, (selected_value1, selected_value2))
        total_epargne = cursor.fetchone()[0]
        #somme des revenus
        cursor.execute(total_revenu_req, (selected_value1, selected_value2))
        total_revenu = cursor.fetchone()[0]
        #somme des dépenses
        cursor.execute(total_depense_req, (selected_value1, selected_value2))
        total_depense = cursor.fetchone()[0]
        #calcul solde
        cursor.execute(solde_req, (selected_value1, selected_value2,selected_value1, selected_value2,selected_value1, selected_value2))
        solde = cursor.fetchone()[0]
        cursor.close()
        connection.close()
        TOTAL_EPARGNE.set(total_epargne)
        TOTAL_REVENU.set(total_revenu)
        TOTAL_DEPENSE.set(total_depense)
        SOLDE.set(solde)

#============================== CRUD Revenu ===================================================================================================
    def onSelectedRevenu(self,event):
        iid = app.revenuTreeview.focus()
        ID_REVENU.set(app.revenuTreeview.set(iid, "id"))
        MOIS_REVENU.set(app.revenuTreeview.set(iid, "moisMvt"))
        ANNEE_REVENU.set(app.revenuTreeview.set(iid, "anneeMvt"))
        LIBELLE_REVENU.set(app.revenuTreeview.set(iid, "nom"))
        MONTANT_REVENU.set(app.revenuTreeview.set(iid, "montant"))
        print (app.revenuTreeview.set(iid, "id"))
        print (app.revenuTreeview.set(iid, "moisMvt"))

    def updateRevenu(self):
        selected_value1 = self.month_combobox.get()
        selected_value2 = self.year_combobox.get()
        #print(ID_REVENU.get())
        #Avant l'exécution de l'instruction, ctrl que les zones de saisies soient remplies
        if MOIS_REVENU.get() == "" or ANNEE_REVENU.get() == "" \
                                    or LIBELLE_REVENU.get() == ""\
                                    or MONTANT_REVENU.get() == "":
            messagebox.showwarning(title='Attention', message="Veuillez rensigner tous les champs")
        else:
            #Suppression des données du treeview
            app.revenuTreeview.delete(*app.revenuTreeview.get_children())
            #Mise à jour dans la BDD
            revenu_req ="UPDATE mouvement SET moisMvt = %s, anneeMvt = %s, libelle = %s, montant = %s WHERE id = %s"
            connection = connexion_database()
            cursor = connection.cursor()
            cursor.execute(revenu_req, (MOIS_REVENU.get(),ANNEE_REVENU.get(),LIBELLE_REVENU.get(),MONTANT_REVENU.get(),ID_REVENU.get()))
            connection.commit()
            #Mise à jour du treeview
            revenu_req = "select id, moisMvt, anneeMvt, montant, libelle from mouvement mvt where mvt.id_type=1 and moisMvt=%s and anneeMvt=%s "
            connection = connexion_database()
            cursor = connection.cursor()
            cursor.execute(revenu_req, (selected_value1, selected_value2))
            revenu_rows = cursor.fetchall()
            for revenu_row in revenu_rows:
                app.revenuTreeview.insert('', "end", values=(
                revenu_row[0], revenu_row[1], revenu_row[2], revenu_row[3], revenu_row[4]))
            #Remise à vide des champs de saisie
            ID_REVENU.set(None)
            MOIS_REVENU.set("")
            ANNEE_REVENU.set("")
            LIBELLE_REVENU.set("")
            MONTANT_REVENU.set("")
            #Message Success
            messagebox.showinfo(title="Success", message="Mise à jour des données réussie")
    def delete_revenu(self):
        selected_value1 = self.month_combobox.get()
        selected_value2 = self.year_combobox.get()
        if not app.revenuTreeview.selection():
            messagebox.showwarning(title="Attention", message="Vous devez sélectionner un enregistrement !")
        else :
            result = messagebox.askquestion(title='Attention', message="Etes-vous sûre de supprimer ce mouvement ?",)
            if result == 'yes':
                iid = app.revenuTreeview.focus()
                numasuppr = app.revenuTreeview.set(iid, "id")
                app.revenuTreeview.delete(iid)
                # Mise à jour dans la BDD
                revenu_req = "DELETE FROM mouvement WHERE id = %s"
                connection = connexion_database()
                cursor = connection.cursor()
                cursor.execute(revenu_req,(numasuppr,))
                connection.commit()
                messagebox.showinfo(title="Succes", message="L'enregistrement a été supprimé ")

# ============================== CRUD Depense ===================================================================================================
    def onSelectedDepense(self, event):
        iid = app.depenseTreeview.focus()
        ID_DEPENSE.set(app.depenseTreeview.set(iid, "id"))
        MOIS_DEPENSE.set(app.depenseTreeview.set(iid, "moisMvt"))
        ANNEE_DEPENSE.set(app.depenseTreeview.set(iid, "anneeMvt"))
        LIBELLE_DEPENSE.set(app.depenseTreeview.set(iid, "nom"))
        MONTANT_DEPENSE.set(app.depenseTreeview.set(iid, "montant"))
        print(app.depenseTreeview.set(iid, "id"))
        print(app.depenseTreeview.set(iid, "moisMvt"))

    def delete_depense(self):
        selected_value1 = self.month_combobox.get()
        selected_value2 = self.year_combobox.get()
        if not app.depenseTreeview.selection():
            messagebox.showwarning(title="Attention", message="Vous devez sélectionner un enregistrement !")
        else :
            result = messagebox.askquestion(title='Attention', message="Etes-vous sûre de supprimer ce mouvement ?",)
            if result == 'yes':
                iid = app.depenseTreeview.focus()
                numasuppr = app.depenseTreeview.set(iid, "id")
                app.depenseTreeview.delete(iid)
                # Mise à jour dans la BDD
                depense_req = "DELETE FROM mouvement WHERE id = %s"
                connection = connexion_database()
                cursor = connection.cursor()
                cursor.execute(depense_req,(numasuppr,))
                connection.commit()
                messagebox.showinfo(title="Succes", message="L'enregistrement a été supprimé ")
    def updateDepense(self):
        selected_value1 = self.month_combobox.get()
        selected_value2 = self.year_combobox.get()
        print(ID_DEPENSE.get())
        print(MONTANT_DEPENSE.get())
        #Avant l'exécution de l'instruction, ctrl que les zones de saisies soient remplies
        if MOIS_DEPENSE.get() == "" or ANNEE_DEPENSE.get() == "" \
                                    or LIBELLE_DEPENSE.get() == ""\
                                    or MONTANT_DEPENSE.get() == "":
            messagebox.showwarning(title='Attention', message="Veuillez rensigner tous les champs")
        else:
            #Suppression des données du treeview
            app.depenseTreeview.delete(*app.depenseTreeview.get_children())
            #Mise à jour dans la BDD
            depense_req ="UPDATE mouvement SET moisMvt = %s, anneeMvt = %s, libelle = %s, montant = %s WHERE id = %s"
            connection = connexion_database()
            cursor = connection.cursor()
            cursor.execute(depense_req, (MOIS_DEPENSE.get(),ANNEE_DEPENSE.get(),LIBELLE_DEPENSE.get(),MONTANT_DEPENSE.get(),ID_DEPENSE.get()))
            connection.commit()
            #Mise à jour du treeview
            depense_req = "select id, moisMvt, anneeMvt, montant, libelle from mouvement mvt where mvt.id_type=2 and moisMvt=%s and anneeMvt=%s "
            connection = connexion_database()
            cursor = connection.cursor()
            cursor.execute(depense_req, (selected_value1, selected_value2))
            depense_rows = cursor.fetchall()
            for depense_row in depense_rows:
                app.depenseTreeview.insert('', "end", values=(
                depense_row[0], depense_row[1], depense_row[2], depense_row[3], depense_row[4]))
            #Remise à vide des champs de saisie
            ID_DEPENSE.set(None)
            MOIS_DEPENSE.set("")
            ANNEE_DEPENSE.set("")
            LIBELLE_DEPENSE.set("")
            MONTANT_DEPENSE.set("")
            #Message Success
            messagebox.showinfo(title="Succes", message="Mise à jour des données réussie")

# ============================== CRUD Epargne ===================================================================================================
    def onSelectedEpargne(self, event):
        iid = app.epargneTreeview.focus()
        ID_EPARGNE.set(app.epargneTreeview.set(iid, "id"))
        MOIS_EPARGNE.set(app.epargneTreeview.set(iid, "moisMvt"))
        ANNEE_EPARGNE.set(app.epargneTreeview.set(iid, "anneeMvt"))
        MONTANT_EPARGNE.set(app.epargneTreeview.set(iid, "montant"))
        print(app.epargneTreeview.set(iid, "id"))
        print(app.epargneTreeview.set(iid, "moisMvt"))

    def delete_epargne(self):
        selected_value1 = self.month_combobox.get()
        selected_value2 = self.year_combobox.get()
        if not app.epargneTreeview.selection():
            messagebox.showwarning(title="Attention", message="Vous devez sélectionner un enregistrement !")
        else :
            result = messagebox.askquestion(title='Attention', message="Etes-vous sûre de supprimer ce mouvement ?",)
            if result == 'yes':
                iid = app.epargneTreeview.focus()
                numasuppr = app.epargneTreeview.set(iid, "id")
                app.epargneTreeview.delete(iid)
                # Mise à jour dans la BDD
                epargne_req = "DELETE FROM mouvement WHERE id = %s"
                connection = connexion_database()
                cursor = connection.cursor()
                cursor.execute(epargne_req,(numasuppr,))
                connection.commit()
                messagebox.showinfo(title="Succes", message="L'enregistrement a été supprimé ")
    def updateEpargne(self):
        selected_value1 = self.month_combobox.get()
        selected_value2 = self.year_combobox.get()
        print(ID_EPARGNE.get())
        print(MONTANT_EPARGNE.get())
        #Avant l'exécution de l'instruction, ctrl que les zones de saisies soient remplies
        if MOIS_EPARGNE.get() == "" or ANNEE_EPARGNE.get() == "" \
                                    or MONTANT_EPARGNE.get() == "":
            messagebox.showwarning(title='Attention', message="Veuillez rensigner tous les champs")
        else:
            #Suppression des données du treeview
            app.epargneTreeview.delete(*app.epargneTreeview.get_children())
            #Mise à jour dans la BDD
            epargne_req ="UPDATE mouvement SET moisMvt = %s, anneeMvt = %s, montant = %s WHERE id = %s"
            connection = connexion_database()
            cursor = connection.cursor()
            cursor.execute(epargne_req, (MOIS_EPARGNE.get(),ANNEE_EPARGNE.get(),MONTANT_EPARGNE.get(),ID_EPARGNE.get()))
            connection.commit()
            #Mise à jour du treeview
            epargne_req = "select id, moisMvt, anneeMvt, montant from mouvement mvt where mvt.id_type=3 and moisMvt=%s and anneeMvt=%s "
            connection = connexion_database()
            cursor = connection.cursor()
            cursor.execute(epargne_req, (selected_value1, selected_value2))
            epargne_rows = cursor.fetchall()
            for epargne_row in epargne_rows:
                app.epargneTreeview.insert('', "end", values=(
                epargne_row[0], epargne_row[1], epargne_row[2], epargne_row[3]))
            #Remise à vide des champs de saisie
            ID_EPARGNE.set(None)
            MOIS_EPARGNE.set("")
            ANNEE_EPARGNE.set("")
            MONTANT_EPARGNE.set("")
            #Message Success
            messagebox.showinfo(title="Succes", message="Mise à jour des données réussie")

    def quitter_application(self):
        if messagebox.askokcancel("Quitter", "Voulez-vous vraiment quitter l'application ?"):
            # Ferme la fenêtre principale de l'application
            self.master.destroy()

#Fenêtre Principale
root = ctk.CTk()
root.title("Monetto")
root.config(background="#EAF2F8")
#root.iconbitmap('asset/argent.ico')
# Définir la taille de la fenêtre à l'ouverture

root.geometry("1580x900")

# ===============Definition des variables de saisies ====================================
ID_REVENU= tk.IntVar()
MOIS_REVENU= tk.StringVar()
ANNEE_REVENU= tk.StringVar()
LIBELLE_REVENU= tk.StringVar()
MONTANT_REVENU= tk.StringVar()

ID_DEPENSE= tk.IntVar()
MOIS_DEPENSE= tk.StringVar()
ANNEE_DEPENSE= tk.StringVar()
LIBELLE_DEPENSE= tk.StringVar()
MONTANT_DEPENSE= tk.StringVar()

ID_EPARGNE= tk.IntVar()
MOIS_EPARGNE= tk.StringVar()
ANNEE_EPARGNE= tk.StringVar()
MONTANT_EPARGNE= tk.StringVar()

TOTAL_EPARGNE =tk.DoubleVar()
TOTAL_REVENU =tk.DoubleVar()
TOTAL_DEPENSE =tk.DoubleVar()
SOLDE = tk.DoubleVar()

#============================================================================================

# récupération du mois et de l'année en cours
annee_actuelle = datetime.datetime.now().year
mois_actuel = datetime.datetime.now().strftime("%B")

# Généreration d'une liste de 5 ans glissants
liste_annees = [str(year) for year in range(annee_actuelle - 5, annee_actuelle + 6)]

#=============================================================================================
# instance de la classe myApplication
app = MyApplication(master=root)

#===================================== Affichage Commun ===============================================================
# Ajouter le titre de bienvenue
app.monBudget_label = tk.Label(root, text="Mon Budget", bg="#2471A3", font=("Arial", 17), padx=60, pady=5, fg="white")
app.monBudget_label.grid(column=0, row=0, columnspan=2)

#Selectionner le mois et l'année
months = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"]
app.month_combobox = ctk.CTkComboBox(root, values=months)
app.month_combobox.grid(row=0, column=5, padx=20, pady=20, sticky="nsew")
app.month_combobox.set(mois_actuel)

app.year_combobox = ctk.CTkComboBox(root, values=liste_annees)
app.year_combobox.grid(row=0, column=6, padx=20, pady=20, sticky="nsew")
app.year_combobox.set(annee_actuelle)

selection = ctk.CTkButton(root, text="Afficher", command=app.afficher_donnees)
selection.grid(row=0, column=7, padx=20, pady=20, sticky="nsew")

#====================================Affichage Partie Revenu =========================================================
revenus_label = tk.Label(root, text="Mes revenus", fg="#51525D", bg="#EAF2F8", font=("Arial", 17), padx=5, pady=5)
revenus_label.grid(column=0, row=1, columnspan=2)
#=================================== Troisième ligne partie gauche ===================================================
# ============================= Affichage des revenus dans le treeview ===============================================
# Afficher le résultat de la requete
app.revenuTreeview = ttk.Treeview(root, columns=("id","moisMvt","anneeMvt", "montant", "nom"))
        # ajouter des colonne avec une largeur définie
app.revenuTreeview.column("#0", width=0, stretch=False)
app.revenuTreeview.column("id", width=50, )
app.revenuTreeview.column("moisMvt", width=80, )
app.revenuTreeview.column("anneeMvt", width=80, )
app.revenuTreeview.column("montant", width=80,)
        # Ajouter les entêtes de colonne
app.revenuTreeview.heading("#0", text="ID" )
app.revenuTreeview.heading("id", text="Id")
app.revenuTreeview.heading("moisMvt", text="mois")
app.revenuTreeview.heading("anneeMvt", text="annee")
app.revenuTreeview.heading("montant", text="montant")
app.revenuTreeview.heading("nom", text="libelle")

# afficher résultat de la requete sql et les ajouter au widget treeview
app.revenuTreeview.grid(row=2, column=0, columnspan=4, rowspan=3)
app.revenuTreeview.bind('<Double-Button-1>', app.onSelectedRevenu)

# =================================== 4eme ligne partie droite ===================se====================================
montantRevenu_label = tk.Label(root, text="Total revenus :", bg="#F7DC67", fg="#51525D", font=("Arial", 14), padx=15, pady=5)
montantRevenu_label.grid(column=1, row=5)

app.montant_label = tk.Label(root, textvariable=TOTAL_REVENU, padx=15, pady=5, bg="#F7DC67", fg="#51525D", font=("Arial", 14))
app.montant_label.grid(column=2, row=5)

app.ajoutRevenu_Bouton = ctk.CTkButton(root, text="Ajouter", command=ouvrir_form_ajout_revenu)
app.ajoutRevenu_Bouton.grid(column=4, row=3)

# =================================== 5eme ligne partie droite =======================================================
app.SuppRevenu_Bouton = ctk.CTkButton(root, text="Supprimer", command=app.delete_revenu)
app.SuppRevenu_Bouton.grid(column=4, row=4)

mois_revenu_label = tk.Label(root, text="Mois", font=("Arial", 12), fg="#51525D", bg="#EAF2F8")
mois_revenu_label.grid(column=0, row=6)

annee_revenu_label = tk.Label(root, text="Annee", font=("Arial", 12), fg="#51525D", bg="#EAF2F8")
annee_revenu_label.grid(column=1, row=6)

libelle_revenu_label = tk.Label(root, text="Libelle", bg="#EAF2F8", fg="#51525D", font=("Arial", 12))
libelle_revenu_label.grid(column=2, row=6)

montant_revenu_label = tk.Label(text="Montant", bg="#EAF2F8", fg="#51525D", font=("Arial", 12))
montant_revenu_label.grid(column=3, row=6)

# =================================== 7eme ligne partie droite =======================================================
app.mois_revenu = ctk.CTkEntry(root, textvariable=MOIS_REVENU)
app.mois_revenu.grid(column=0, row=7)

annee_revenu = ctk.CTkEntry(root, textvariable=ANNEE_REVENU)
annee_revenu.grid(column=1, row=7)

libelle_revenu = ctk.CTkEntry(root, textvariable=LIBELLE_REVENU)
libelle_revenu.grid(column=2, row=7)

montant_revenu = ctk.CTkEntry(root, textvariable=MONTANT_REVENU)
montant_revenu.grid(column=3, row=7)

app.ModifRevenu_Bouton = ctk.CTkButton(root, text="Modifier", command=app.updateRevenu)
app.ModifRevenu_Bouton.grid(column=4, row=7)

#====================================Affichage partie Dépense ========================================================
# =================================== 8eme ligne partie droite =======================================================
depenses_label = tk.Label(root, text="Mes dépenses", fg="#51525D", bg="#EAF2F8", font=("Arial", 17), padx=5, pady=5)
depenses_label.grid(column=0, row=8, columnspan=2)

# =================================== 9eme ligne partie droite =======================================================
# ============================= Affichage des depenses dans le treeview ===============================================
# Afficher le résultat de la requete
app.depenseTreeview = ttk.Treeview(root, columns=("id", "moisMvt", "anneeMvt", "montant", "nom"), style="Treeview")
# ajouter des colonne avec une largeur définie
app.depenseTreeview.column("#0", width=0, stretch=False)
app.depenseTreeview.column("id", width=50, stretch=False)
app.depenseTreeview.column("moisMvt", width=80, stretch=False)
app.depenseTreeview.column("anneeMvt", width=80, stretch=False)
app.depenseTreeview.column("montant", width=80, stretch=True)
# Ajouter les entêtes de colonne
app.depenseTreeview.heading("#0", text="ID")
app.depenseTreeview.heading("id", text="Id")
app.depenseTreeview.heading("moisMvt", text="mois")
app.depenseTreeview.heading("anneeMvt", text="mois")
app.depenseTreeview.heading("montant", text="montant")
app.depenseTreeview.heading("nom", text="libelle")

# Afficher le résultat de la requete dans le treeview
app.depenseTreeview.grid(row=9, column=0, columnspan=4, rowspan=3)
app.depenseTreeview.bind('<Double-Button-1>', app.onSelectedDepense)

# =================================== 10eme ligne partie droite =======================================================
montantDepense_label = tk.Label(root, text="Total dépenses :", bg="#F7DC67", fg="#51525D", font=("Arial", 14), padx=15, pady=5)
montantDepense_label.grid(column=1, row=12)

app.depense_label = tk.Label(root, textvariable=TOTAL_DEPENSE, padx=15, pady=5, bg="#F7DC67", fg="#51525D", font=("Arial", 14))
app.depense_label.grid(column=2, row=12)

app.AjoutDepense_Bouton = ctk.CTkButton(root, text="Ajouter", command=ouvrir_form_ajout_depense)
app.AjoutDepense_Bouton.grid(column=4, row=10, rowspan=1,)

# =================================== 11eme ligne partie droite =======================================================
SuppDepense_Bouton = ctk.CTkButton(root, text="Supprimer", command=app.delete_depense)
SuppDepense_Bouton.grid(column=4, row=11, rowspan=1,)

# =================================== 12eme ligne partie droite =======================================================
mois_depense_label = tk.Label(root, text="Mois", font=("Arial", 12), fg="#51525D", bg="#EAF2F8")
mois_depense_label.grid(column=0, row=13)

annee_depense_label = tk.Label(root, text="Mois", font=("Arial", 12), fg="#51525D", bg="#EAF2F8")
annee_depense_label.grid(column=1, row=13)

libelle_depense_label = tk.Label(root, text="Mois", font=("Arial", 12), fg="#51525D", bg="#EAF2F8")
libelle_depense_label.grid(column=2, row=13)

montant_depense_label = tk.Label(root, text="Mois", font=("Arial", 12), fg="#51525D", bg="#EAF2F8")
montant_depense_label.grid(column=3, row=13)


# =================================== 13eme ligne partie droite =======================================================
mois_depense = ctk.CTkEntry(root, textvariable=MOIS_DEPENSE )
mois_depense.grid(column=0, row=14,)

annee_depense = ctk.CTkEntry(root, textvariable=ANNEE_DEPENSE)
annee_depense.grid(column=1, row=14,)

libelle_depense = ctk.CTkEntry(root, textvariable=LIBELLE_DEPENSE)
libelle_depense.grid(column=2, row=14)

montant_depense = ctk.CTkEntry(root, textvariable=MONTANT_DEPENSE)
montant_depense.grid(column=3, row=14,)

ModifDepense_Bouton = ctk.CTkButton(root, text="Modifier", command=app.updateDepense)
ModifDepense_Bouton.grid(column=4, row=14,)

#===================================== Affichage epargne ===============================================================
epargne_label = tk.Label(root, text="Mon épargne", fg="#51525D", bg="#EAF2F8", font=("Arial", 17), padx=5, pady=5)
epargne_label.grid(column=5, row=1, columnspan=3)

# ============================= Affichage des epargne dans le treeview ===============================================
# Afficher le résultat de la requete
app.epargneTreeview = ttk.Treeview(root, columns=("id", "moisMvt", "anneeMvt", "montant"), style="Treeview")
# ajouter des colonne avec une largeur définie
app.epargneTreeview.column("#0", width=0, stretch=False)
app.epargneTreeview.column("id", width=50, stretch=False)
app.epargneTreeview.column("moisMvt", width=80, stretch=False)
app.epargneTreeview.column("montant", width=80, stretch=True)
# Ajouter les entêtes de colonne
app.epargneTreeview.heading("#0", text="ID")
app.epargneTreeview.heading("id", text="Id")
app.epargneTreeview.heading("moisMvt", text="mois")
app.epargneTreeview.heading("anneeMvt", text="mois")
app.epargneTreeview.heading("montant", text="montant")
# Afficher le résultat de la requete dans le treeview
app.epargneTreeview.grid(row=2, column=5, columnspan=3, rowspan=3)
app.epargneTreeview.bind('<Double-Button-1>', app.onSelectedEpargne)

app.ajoutEpargne_Bouton = ctk.CTkButton(root, text="Ajouter", command=ouvrir_form_ajout_epargne)
app.ajoutEpargne_Bouton.grid(column=8, row=3)

montantEpargne_label = tk.Label(root, text="Montant épargné :", bg="#F7DC67", fg="#51525D", font=("Arial", 14), padx=15, pady=5)
montantEpargne_label.grid(column=6, row=5)

app.montant_label = tk.Label(root, textvariable=TOTAL_EPARGNE, padx=15, pady=5, bg="#F7DC67", fg="#51525D", font=("Arial", 14))
app.montant_label.grid(column=7, row=5, sticky="w")

# =================================== Epargne label update partie gauche =======================================================
mois_epargne_label = tk.Label(root, text="Mois", font=("Arial", 12), fg="#51525D", bg="#EAF2F8")
mois_epargne_label.grid(column=5, row=6)

annee_epargne_label = tk.Label(root, text="Annee", font=("Arial", 12), fg="#51525D", bg="#EAF2F8")
annee_epargne_label.grid(column=6, row=6)

montant_epargne_label = tk.Label(root, text="Montant", font=("Arial", 12), fg="#51525D", bg="#EAF2F8")
montant_epargne_label.grid(column=7, row=6)

app.SuppEpargne_Bouton = ctk.CTkButton(root, text="Supprimer", command=app.delete_epargne)
app.SuppEpargne_Bouton.grid(column=8, row=4)

# =================================== Epargne Entry update partie gauche =======================================================
mois_epargne = ctk.CTkEntry(root, textvariable=MOIS_EPARGNE )
mois_epargne.grid(column=5, row=7)

annee_epargne = ctk.CTkEntry(root, textvariable=ANNEE_EPARGNE)
annee_epargne.grid(column=6, row=7)

montant_epargne = ctk.CTkEntry(root, textvariable=MONTANT_EPARGNE)
montant_epargne.grid(column=7, row=7)

ModifEpargne_Bouton = ctk.CTkButton(root, text="Modifier", command=app.updateEpargne)
ModifEpargne_Bouton.grid(column=8, row=7)

epargne_label = tk.Label(root, text="En résumé", fg="#51525D", bg="#EAF2F8", font=("Arial", 17), padx=5, pady=20)
epargne_label.grid(column=5, row=8, columnspan=3)

montantEpargne_label = tk.Label(root, text="Montant épargné :", bg="#BBDEA8", fg="#51525D", font=("Arial", 14), padx=15, pady=5)
montantEpargne_label.grid(column=5, row=9, columnspan=2)

app.montant_label = tk.Label(root, textvariable=TOTAL_EPARGNE, padx=15, pady=5, bg="#BBDEA8", fg="#51525D", font=("Arial", 14))
app.montant_label.grid(column=7, row=9)

montantDepense_label = tk.Label(root, text="Total dépenses :", bg="#BBDEA8", fg="#51525D", font=("Arial", 14), padx=15, pady=5)
montantDepense_label.grid(column=5, row=10, columnspan=2)

app.depense_label = tk.Label(root, textvariable=TOTAL_DEPENSE, padx=15, pady=5, bg="#BBDEA8", fg="#51525D", font=("Arial", 14))
app.depense_label.grid(column=7, row=10)

montantRevenu_label = tk.Label(root, text="Total revenus :", bg="#BBDEA8", fg="#51525D", font=("Arial", 14), padx=15, pady=5)
montantRevenu_label.grid(column=5, row=11, columnspan=2)

app.montant_label = tk.Label(root, textvariable=TOTAL_REVENU, padx=15, pady=5, bg="#BBDEA8", fg="#51525D", font=("Arial", 14))
app.montant_label.grid(column=7, row=11)

montantSolde_label = tk.Label(root, text="SOLDE :", bg="#BBDEA8", fg="#51525D", font=("Arial", 14), padx=15, pady=5)
montantSolde_label.grid(column=5, row=13, columnspan=2)

app.solde_label = tk.Label(root, textvariable=SOLDE, padx=15, pady=5, bg="#BBDEA8", fg="#51525D", font=("Arial", 14))
app.solde_label.grid(column=7, row=13)

#========================================= Quitter ====================================================================
app.Quitter_Bouton = ctk.CTkButton(root, text="Quitter", command=app.quitter_application)
app.Quitter_Bouton.grid(column=8, row=15)


#==========================================GRID========================================================================

#Ajouter un poids aux colonnes et aux lignes pour que les widgets prennent toute la place disponibled
#root.columnconfigure(0, weight=0, uniform="same_group")
root.columnconfigure(1, weight=0, uniform="same_group")
root.columnconfigure(2, weight=0, uniform="same_group")
root.columnconfigure(3, weight=0, uniform="same_group")
root.columnconfigure(4, weight=0, uniform="same_group")
root.columnconfigure(5, weight=0, uniform="same_group")
#root.columnconfigure(6, weight=0, uniform="same_group")
#root.columnconfigure(7, weight=0, uniform="same_group")
#root.columnconfigure(8, weight=0, uniform="same_group")


root.rowconfigure(2, weight=0, uniform="same_group")
root.rowconfigure(3, weight=0, uniform="same_group")
root.rowconfigure(4, weight=0, uniform="same_group")
root.rowconfigure(5, weight=0, uniform="same_group")
root.rowconfigure(6, weight=0, uniform="same_group")
root.rowconfigure(7, weight=0, uniform="same_group")
root.rowconfigure(9, weight=0, uniform="same_group")
root.rowconfigure(10, weight=0, uniform="same_group")
root.rowconfigure(11, weight=0, uniform="same_group")
root.rowconfigure(12, weight=0, uniform="same_group")
root.rowconfigure(13, weight=0, uniform="same_group")

#Affichage fenêtre
root.mainloop()
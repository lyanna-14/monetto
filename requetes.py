from connection import connexion_database

def execution_requete(req):
    connection = connexion_database()
    cursor = connection.cursor()
    cursor.execute(req)
    resultat = cursor.fetchall()
    connection.commit()
    cursor.close()
    connection.close()
    return resultat

def lister_revenus():
    revenu_req = "select moisMvt, anneeMvt, montant, libelle from mouvement mvt where mvt.id_type=1"
    revenu_rows = execution_requete(revenu_req)
    return revenu_rows

def lister_depenses():
    depense_req = "select moisMvt, anneeMvt, montant, libelle from mouvement mvt where mvt.id_type=2"
    depense_rows = execution_requete(depense_req)
    return depense_rows

def lister_epargnes():
    epargne_req = "select moisMvt, anneeMvt, montant, libelle from mouvement mvt where mvt.id_type=3"
    epargne_rows = execution_requete(epargne_req)
    return epargne_rows

def lister_libelle_revenu():
    libelle_revenu_req = "select nom from libelle where id_type=1"
    libelle_revenu_rows = execution_requete(libelle_revenu_req)
    return libelle_revenu_rows

def lister_libelle_depense():
    libelle_epargne_req = "select nom from libelle where id_type=2"
    libelle_epargne_rows = execution_requete(libelle_epargne_req )
    return libelle_epargne_rows

def lister_id_mouvement():
    connection = connexion_database()
    cursor = connection.cursor()
    id_mouvement_req = "select max(id)+1 from mouvement"
    cursor.execute(id_mouvement_req)
    id_mouvement = cursor.fetchone()[0]
    cursor.close()
    connection.close()
    return id_mouvement
